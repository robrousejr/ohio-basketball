CREATE TABLE team (
    Team_id INT(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Team_name VARCHAR(30),
    Team_league VARCHAR(30),
    Team_mascot VARCHAR(30), 
    Team_address VARCHAR(50),
    Team_city VARCHAR(50), 
    Team_state VARCHAR(20),
    Team_colors VARCHAR(50)
);

CREATE TABLE season (
    id INT(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    team_id INT(5), 
    season YEAR(4) NOT NULL,
    wins INT(2) DEFAULT 0,
    losses INT(2) DEFAULT 0,
    wlratio FLOAT DEFAULT 0,
    leagueWins INT(2) DEFAULT 0,
    leagueLosses INT(2) DEFAULT 0,
    leagueWLRatio FLOAT DEFAULT 0,
    FOREIGN KEY(team_id) REFERENCES team(Team_id)
);


CREATE TABLE users (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    Team_id INT NOT NULL, 
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    userType INT NOT NULL DEFAULT 2,
    FOREIGN KEY(Team_id) REFERENCES team(Team_id)
);

CREATE TABLE game (
    Game_id int(12) NOT NULL AUTO_INCREMENT,
    Game_team1 int(5) NULL, 
    Game_team2 int(5) NULL,
    Game_team1score int(3) UNSIGNED NULL,
    Game_team2score int(3) UNSIGNED NULL,
    Game_oppName VARCHAR(30) NULL,
    Game_date date NOT NULL,
    Game_season YEAR(4) NOT NULL,
    FOREIGN KEY (Game_team1) REFERENCES team(Team_id), 
    FOREIGN KEY (Game_team2) REFERENCES team(Team_id), 
    PRIMARY KEY(Game_id)
);

INSERT INTO team VALUES 
(NULL, 'Jackson Milton', 'MVAC', "Blue Jays", "13910 Mahoning Ave", "North Jackson", "Ohio", "White, Royal Blue"),
(NULL, 'Lowellville', 'MVAC', "Rockets", "52 Rocket Pl", "Lowellville", "Ohio", "Navy Blue, Gold"),
(NULL, 'McDonald', 'MVAC', "Blue Devils", "600 Iowa Ave", "McDonald", "Ohio", "Blue, White"),
(NULL, 'Mineral Ridge', 'MVAC', "Rams", "1334 Seaborn St", "Mineral Ridge", "Ohio", "Orange, Black"),
(NULL, 'Sebring', 'MVAC', "Trojans", "225 E Indiana Ave", "Sebring", "Ohio", "Purple, Gold"),
(NULL, 'Springfield', 'MVAC', "Tigers", "11335 Youngstown - Pittsburgh Rd", "New Middletown", "Ohio", "Orange, Black"),
(NULL, 'Waterloo', 'MVAC', "Vikings", "1464 Industry Rd", "Atwater", "Ohio", "Crimson, White"),
(NULL, 'Western Reserve', 'MVAC', "Blue Devils", "13850 W Akron-Canfield Rd", "Berlin Center", "Ohio", "White, Blue, Red");