<?php
  // connect to database
  $host = 'localhost';
  $username = 'ispuser';
  $password = 'ispuserpassword';
  $database = 'mvac_db';
  $conn = mysqli_connect($host, $username, $password, $database);

  // check connection
  if(!$conn)
  {
      echo "Connection error: " . mysqli_connect_error(); // output connection error
  }

  date_default_timezone_set('America/Chicago'); // Set timezone
?>