<?php
require_once 'connect.php'; ?>
<?php

// if url has league=xxxx
if (isset($_GET['league'])) {

    // If url has &season=xxxx
    if (isset($_GET['season'])) {
        $seasonNum = mysqli_real_escape_string($conn, $_GET['season']);  // Set seasonNum
    } else {

        // Display current season 
        $month = date("m");
        $year = date("Y");
        if ($month < 7) {
            $seasonNum = $year - 1;
        } else {
            $seasonNum = $year;
        }
    }

    // Holds league string of the team for searching
    $league = mysqli_real_escape_string($conn, $_GET['league']);

    // Find all teams in the league
    $sql = "SELECT * FROM team WHERE Team_league='$league'";
    $result = mysqli_query($conn, $sql); // Stores all table data in query
    $teams = mysqli_fetch_all($result, MYSQLI_ASSOC); // Holds array of teams in the league
    $teamCount = count($teams); // Number of teams in league

    // Iterate through each team
    for($i = 0; $i < $teamCount; $i++){
        $tmpTeamID = $teams[$i]['Team_id'];
        $seasonExistsSQL = "SELECT * FROM season WHERE season=$seasonNum AND team_id=$tmpTeamID";
        $seasonExistsResult = mysqli_query($conn, $seasonExistsSQL);
        $seasonExistsArray = mysqli_fetch_all($seasonExistsResult, MYSQLI_ASSOC);
        $seasonExistsCt = count($seasonExistsArray);

        if($seasonExistsCt == 1){
            // Season exists
            $wlratio = $seasonExistsArray[0]['leagueWLRatio'];
        }
        else {
            // Season doesn't exist
            $wlratio = 0;
        }

        $teams[$i]['wlratio'] = $wlratio; // Add wlratio key to array
    }

    // Sort teams by wlratio
    $wlratioColumn = array_column($teams, 'wlratio');
    array_multisort($wlratioColumn, SORT_DESC, $teams);

    // League name capitalized correctly 
    $leagueCapped = $teams[0]['Team_league'];
}

// Else redirect back to home
else {
    header('Location: index.php');
}

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Alatsi&display=swap" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>

    <title><?php echo $leagueCapped ?> Teams</title>
</head>

<body>
    <!-- Navbar -->
    <?php include('navbar.php'); ?>

    <div class="container mt-5">

        <!-- League Information -->
        <div class="row mb-2 text-center">
            <div class="col my-auto">
                <h1 class="display-4"><?php echo $leagueCapped ?> Teams</h1>
            </div>
        </div>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Teams</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Chatroom</a>
            </li>
        </ul>
        <div class="tab-content mt-4" id="myTabContent">
            <!-- League Teams Tab -->
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <!-- Select Season year -->
                <div class="row">
                    <div class="col-sm">
                        <div class="input-group">
                            <select class="custom-select" id="selectSeason">
                                <?php
                                // Holds league's Team_id's
                                $teamIDArray = [];

                                // Fill array with team ID's in league
                                for ($i = 0; $i < $teamCount; $i++) {
                                    $tmpTeamID = $teams[$i]['Team_id'];
                                    array_push($teamIDArray, $tmpTeamID);
                                }

                                $TeamIDArrayIN = '(' . implode(',', $teamIDArray) . ')'; // Allows SQL to find values in array

                                // Select all seasons played by league teams
                                $leagueSeasonSQL = "SELECT DISTINCT season FROM season WHERE team_id IN " . $TeamIDArrayIN;
                                $leagueSeasonResult = mysqli_query($conn, $leagueSeasonSQL);
                                $leagueSeasonsResultArray = mysqli_fetch_all($leagueSeasonResult, MYSQLI_ASSOC); // Holds each unique season with league info 

                                // Sort season order decreasing
                                $leagueSeasons = [];
                                foreach ($leagueSeasonsResultArray as $season) {
                                    array_push($leagueSeasons, $season['season']); // Fill array with all league seasons 
                                }
                                arsort($leagueSeasons); // Sort season order by decreasing

                                // Output season option values
                                foreach ($leagueSeasons as $season) {
                                    echo "<option value='" . $season . "'>" . $season . "-" . ($season + 1) . "</option>";
                                }

                                echo
                                    '
                                <script>
                                    // Select current season option
                                    var seasonNum = document.getElementById("selectSeason").value="' . $seasonNum . '";
                                    seasonNum.selected = true;
                                </script>
                                ';

                                ?>
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="updateScheduleYear()">View</button>
                            </div>
                        </div>
                    </div>
                    <div class="col"></div>
                    <div class="col"></div>
                </div>

                <div class="container">
                    <table class="table table-bordered mb-5 mt-3">
                        <thead class="thead border-bottom">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Team</th>
                                <th scope="col">League Record</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            // Iterate through teams
                            for ($i = 0; $i < $teamCount; $i++) {

                                $rowNum = $i + 1; // Row number for the table ('#' col);
                                $teamID = $teams[$i]['Team_id'];
                                $teamName = $teams[$i]['Team_name'];

                                // Display season win/loss information
                                $seasonSQL = "SELECT leagueWins, leagueLosses FROM season WHERE team_id=$teamID AND season=$seasonNum";
                                $seasonSQLResult = mysqli_query($conn, $seasonSQL);
                                $seasonSQLArray = mysqli_fetch_all($seasonSQLResult, MYSQLI_ASSOC);

                                // Define win count
                                $teamWins = 0;
                                if (!sizeof($seasonSQLArray) == 0 && $seasonSQLArray[0]['leagueWins']) {
                                    $teamWins = $seasonSQLArray[0]['leagueWins'];
                                }

                                // Define loss count
                                $teamLosses = 0;
                                if (!sizeof($seasonSQLArray) == 0 && $seasonSQLArray[0]['leagueLosses']) {
                                    $teamLosses = $seasonSQLArray[0]['leagueLosses'];
                                }

                                // Output team list 
                                echo
                                    "
                                <tr>
                                <td scope='row'>$rowNum</td>
                                <td scope='row'><a href='./team.php?id=$teamID'>$teamName</a></td>
                                <td scope='row'>$teamWins-$teamLosses</td>
                                </tr>
                                ";
                            }

                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- Chatroom Tab -->
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <iframe src="https://minnit.chat/MVAC?embed&nickname=" style="border:none;width:100%;height:500px;" allowTransparency="true"></iframe><br><a href="https://minnit.chat/MVAC" target="_blank">
            </div>
        </div>
    </div>

    <script src="js/functions.js"></script>
    <script>
        // Update schedule year link
        function updateScheduleYear() {
            var selectSeasonDropdown = document.getElementById("selectSeason");
            var seasonSelected = selectSeasonDropdown.options[selectSeasonDropdown.selectedIndex].value; // Value of selected season

            // Get new URL and redirect
            var newURL = updateURLParameter(window.location.href, 'season', seasonSelected);
            window.location.href = newURL;
        }
    </script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>