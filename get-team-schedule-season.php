<!-- AJAX returns schedule in table format --> 
<?php require_once 'connect.php'; ?>
<?php include('functions.php')?>
<?php

$teamname = $_GET['teamname'];
$season = $_GET['season'];

$teamSQL = "SELECT Team_id FROM team WHERE Team_name='$teamname' LIMIT 1";
$teamResult = mysqli_fetch_all(mysqli_query($conn, $teamSQL));
$teamID = $teamResult[0][0]; // Holds team ID of main team

$gamesSQL = "SELECT * FROM game WHERE (Game_team1=$teamID OR Game_team2=$teamID) AND Game_season=$season";
$games = mysqli_fetch_all(mysqli_query($conn, $gamesSQL)); // Holds array of games in chosen season

$gameCount = count($games); // Number of games played in total

echo 
"
<div class='container' id='scheduleTableContainer'>

<!-- Start of table -->
<table class='table table-bordered mb-5 mt-3'>
    <thead class='thead border-bottom'>
        <tr>
            <th scope='col'>Date</th>
            <th scope='col'>Opponent</th>
            <th scope='col'>Score</th>
            <th scope='col'>Action</th>
        </tr>
    </thead>
    <tbody>
    <!-- Fill table with games -->
";

// Tracking record
$winCountOverall = 0;
$lossCountOverall = 0;


// Output table
for ($i = 0; $i < $gameCount; $i++) {

    // Sets date of game in schedule format
    $gameDate = formatDateForSchedule($games[$i][6]);

    $winOrLoss = ''; // Holds 'W' or 'L'

    // Holds teams ID's
    $team1 = $games[$i][1];
    $team2 = $games[$i][2];

    // Holds team scores
    $team1Score = $games[$i][3];
    $team2Score = $games[$i][4];

    $gameID = $games[$i][0];

    // Holds 1 or 2 depending on which team is shown
    $team1or2 = thisTeam($team1, $team2, $teamID);

    // Final score
    $score = '';

    // Set opponent ID
    if ($team1or2 == 1) {
        $opponentID = $team2; // Holds Team_id of opposing team
    } else {
        $opponentID = $team1; // Holds Team_id of opposing team
    }

    // If opponent isn't in database
    if ($opponentID == '') {
        $opponent = $games[$i][5];
    }
    // If opponent is in database
    else {
        $opponentSQL = "SELECT Team_name FROM team WHERE Team_id='$opponentID'";
        $opponentResult = mysqli_query($conn, $opponentSQL);
        $OpponentArray = mysqli_fetch_all($opponentResult, MYSQLI_ASSOC);
        $opponent = $OpponentArray[0]['Team_name']; // Holds opponent name 
    }

    // If game has been played
    if (isGamePlayed($team1Score, $team2Score)) {
        $score = getScore($team1Score, $team2Score, $team1or2, $winOrLoss); // In format 'W 49-40'
    }

    // If score isn't empty
    if ($score != '') {
        if ($score[0] == 'W') {
            $winCountOverall++;
        } else if ($score[0] == 'T') {
        } else {
            $lossCountOverall++;
        }
    }

    // Outputs HTML row data
    scheduleOutputRowDashboard($gameDate, $opponentID, $opponent, $score, $gameID);
}

echo 
"
    </tbody>
</table>
</div>
";

?>