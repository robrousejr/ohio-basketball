<?php require_once 'connect.php'; ?>
<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

?>

<!-- Access Coach data-->
<?php
$id = $_SESSION['id']; // User ID

$sql = "SELECT Team_id, userType from users WHERE id=$id";
$result = mysqli_query($conn, $sql); // Stores all table data in query
$team = mysqli_fetch_all($result, MYSQLI_ASSOC); // Holds array of teams in the league
$team_id = $team[0]['Team_id']; // Holds ID of team that coach coaches
$userType = $team[0]['userType']; // Holds value of coach being admin or not (int)

// Add session variables for team_id and is_admin
$_SESSION['team_id'] = $team_id; 
$_SESSION['userType'] = $userType; 

// Find Team information for dashboard team
$sql = "SELECT Team_name, Team_league, Team_mascot from team WHERE Team_id=$team_id";
$result = mysqli_query($conn, $sql);
$team = mysqli_fetch_all($result, MYSQLI_ASSOC);
$team_name = $team[0]['Team_name']; // Holds dashboard team name 
$team_mascot = $team[0]['Team_mascot'];
$league_name = $team[0]['Team_league']; // Holds league name

// Find any game involving the team and order by date
$sql = "SELECT * FROM game WHERE Game_team1='$team_id' or Game_team2='$team_id' ORDER BY Game_date";
$result = mysqli_query($conn, $sql); // Stores all table data in query
$games = mysqli_fetch_all($result, MYSQLI_ASSOC); // Holds array of games played by dashboard team

$gameCount = count($games); // Number of games played in total
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Alatsi&display=swap" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link href="css/dashboard.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"></link>

    <script src="https://kit.fontawesome.com/2133643ab5.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
    <title>
        <?php 
            if($userType == 1)
            {
                echo "Admin Dashboard";
            }
            else
            {      
                echo "$team_name Dashboard";
            }
        ?>
    </title>
</head>

<body>

    <!-- Navbar -->
    <?php include('navbar.php'); ?>
    <?php include('functions.php'); ?>

    <?php 

        // Get current date
        $dateObj = getdate();
        $currYear = $dateObj['year'];
        $currDay = sprintf("%02d",$dateObj['mday']);
        $currMon = sprintf("%02d",$dateObj['mon']);
        $dateVal = "$currYear-$currMon-$currDay"; // Outputs date in SQL format


        /**
         * If COACH, display team schedule and #addGameAccordions
         */
        if($userType == 0)
        {
            // Output "team_name team_mascot Dashboard"
            echo
            "
                <div class='container'>
                <div class='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom mainFormatDiv'>
                    <h2 class='h2 dashboardTitle'>$team_name $team_mascot Dashboard</h2>
                    <div class='btn-toolbar mb-2 mb-md-0'>
                        <button class='btn btn-warning helpBtn' type='button' data-toggle='modal' data-target='#helpModal'>Help</button>
                        <a href='logout.php'><button class='btn btn-danger ml-2' type='button'>Logout</button></a>
        
                        <!-- Help Modal -->
                        <div class='modal fade' id='helpModal' tabindex='-1' role='dialog' aria-labelledby='helpModal' aria-hidden='true'>
                            <div class='modal-dialog' role='document'>
                                <div class='modal-content'>
                                    <div class='modal-header'>
                                        <h5 class='modal-title' id='exampleModalLabel'>Dashboard Help</h5>
                                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                        </button>
                                    </div>
                                    <div class='modal-body'>
                                        <h1 class='h3'>How to Add Games</h1>
                                        <p class='text-muted'> 
                                            To add games, simply go to your <b>Dashboard</b> and scroll to the bottom where you fill find an \"Add Games\" accordion. From there you can input the necessary data for a game, in which it will automatically be categorized to a season according to the date.
                                        </p>
                                        
                                        <h1 class='h3'>How to Delete Games</h1>
                                        <p class='text-muted'>
                                            Since you are a <b>COACH</b> user. You can only delete games for your team in the <b>Dashboard</b> by clicking the \"Delete\" button in the schedule. This will only be shown if there are games loaded into the database.
                                        </p>

                                        <h1 class='h3'>How to Become a Coach/Admin</h1>
                                        <p class='text-muted'>
                                            To become a <b>COACH</b> or <b>ADMIN</b> for the site, <a href='mailto:example@gmail.com'>contact an administrator</a> and they can grant you permission to become one.
                                        </p>
        
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Team Schedule Viewer -->
                <div class='container' id='coachTeamScheduleViewer'>
                    <h2 class='text-center'>Schedule</h2>

                    <div class='container' id='selectSeasonSpot'>

                    </div>
                    <div id='scheduleTableSpot'>

                    </div>
                </div>

            ";

            echo
            "
                <!-- Add Game -->
                <div class='accordion container mb-3' id='addGameAccordion'>
                <div class='card'>
                    <div class='card-header' id='headingOne'>
                        <h2 class='mb-0'>
                            <button class='btn btn-link' type='button' data-toggle='collapse' data-target='#collapseOne' aria-expanded='false' aria-controls='collapseOne'>
                                Add Game
                            </button>
                        </h2>
                    </div>

                    <div id='collapseOne' class='collapse' aria-labelledby='headingOne' data-parent='#addGameAccordion'>
                        <div class='card-body'>
                            <!-- ADD GAME -->
                            <h2 class='text-center'>Add Game to Schedule</h2>

                            <!-- Date, Opponent, Score, Opponent Score-->
            ";
            echo 
            "
                            <!-- Insert Game Form -->
                            <form method='POST' onsubmit='return validateForm()' action='addgame.php'>
                                <div class='form-group row addGameRow'>
                                    <div class='col'>
                                        <label for='date-input'>Date</label>
                                        <input class='form-control' type='date' name='date' value='$dateVal' id='date-input'>
                                    </div>
                                    <div class='col search-box'>
                                        <label for='opponent-input'>Opponent</label>
                                        <input class='form-control team-input' name='opponent' type='text' id='opponent-input'>
                                    </div>
                                </div>
                                <div class='form-group row addGameRow'>
                                    <div class='col'>
                                        <label for='score-input'>$team_name Score</label>
                                        <input type='number' name='score-input' id='score-input' class='form-control'>
                                    </div>
                                    <div class='col'>
                                        <label for='opp-score-input'>Opponent Score</label>
                                        <input type='number' name='opp-score-input' id='opp-score-input' class='form-control'>
                                    </div>
                                </div>
                                <div class='submitDiv'>
                                    <button type='submit' class='btn btn-primary'>Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            ";
        }

        /**
         * IF ADMIN, display #globalAddGameAccordion to add game for any team
         */
        elseif($userType == 1)
        {
            echo 
            "
            <div class='container'>
                <div class='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom'>
                    <h1 class='h2'>Admin Dashboard</h1>
                    <div class='btn-toolbar mb-2 mb-md-0'>
                        <button class='btn btn-warning' type='button' data-toggle='modal' data-target='#helpModal'>Help</button>
                        <a href='logout.php'><button class='btn btn-danger ml-2' type='button'>Logout</button></a>
        
                        <!-- Help Modal -->
                        <div class='modal fade' id='helpModal' tabindex='-1' role='dialog' aria-labelledby='helpModal' aria-hidden='true'>
                            <div class='modal-dialog' role='document'>
                                <div class='modal-content'>
                                    <div class='modal-header'>
                                        <h5 class='modal-title' id='exampleModalLabel'>Dashboard Help</h5>
                                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                        </button>
                                    </div>
                                    <div class='modal-body'>
                                        <h1 class='h3'>How to Add Games</h1>
                                        <p class='text-muted'> 
                                            To add games, simply go to your <b>Dashboard</b> and scroll to the bottom where you fill find an \"Add Games\" accordion. From there you can input the necessary data for a game, in which it will automatically be categorized to a season according to the date.
                                        </p>
                                        
                                        <h1 class='h3'>How to Delete Games</h1>
                                        <p class='text-muted'>
                                            Since you are a <b>COACH</b> user. You can only delete games for your team in the <b>Dashboard</b> by clicking the \"Delete\" button in the schedule. This will only be shown if there are games loaded into the database.
                                        </p>

                                        <h1 class='h3'>How to Add/Remove a Coach/Admin</h1>
                                        <p class='text-muted'>
                                            To add or remove a <b>COACH</b> or <b>ADMIN</b> for the site, visit <a href='myaccount.php'>My Account</a> and you can add them via username.
                                        </p>
        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ";

            echo 
            "
            <div class='container'>
                <!-- ADD GAME -->
                <h2 class='text-center'>Add Game to Schedule</h2>

                <!-- Insert Game Form -->
                <form method='POST' onsubmit='return validateGlobalForm()' action='adminAddGame.php'>
                    <div class='form-group row addGameRow'>
                        <div class='col search-box'>
                            <label for='opponent-input'>Team</label>
                            <input class='form-control team-input' name='team' type='text' id='team-input'>
                            <small id='emailHelp' class='form-text text-muted'>Team must exist in database</small>
                        </div>
                        <div class='col search-box'>
                            <label for='opponent-input'>Opponent</label>
                            <input class='form-control team-input' name='opponent' type='text' id='opponent-input'>
                        </div>
                    </div>
                    <div class='form-group row addGameRow'>
                        <div class='col'>
                            <label for='date-input'>Date</label>
                            <input class='form-control' type='date' name='date' value='$dateVal' id='date-input'>
                        </div>
                        <div class='col'>
                            <label for='score-input'>Team Score</label>
                            <input type='number' name='score-input' id='score-input' class='form-control'>
                        </div>
                        <div class='col'>
                            <label for='opp-score-input'>Opponent Score</label>
                            <input type='number' name='opp-score-input' id='opp-score-input' class='form-control'>
                        </div>
                    </div>
                    <div class='submitDiv'>
                        <button type='submit' class='btn btn-primary'>Submit</button>
                    </div>
                </form>
            </div>
            ";


            // Global Schedule Viewer
            echo 
            "   
                <hr>         
                <!-- Global Schedule Viewer -->
                <div class='container mt-5'>
                    <h2 class='text-center'>Schedule Viewer</h2>

                    <div class='input-group mb-3 mt-3 col search-box'>
                        <input type='text' class='form-control team-input' name='opponent' placeholder='Team name' id='schedule-search-team-input'>
                        <div class='input-group-append'>
                            <button class='btn btn-outline-secondary' type='button' onclick='loadScheduleSeasonSelector()'>Show Schedule</button>
                        </div>
                    </div>
                    <div class='container'id='selectSeasonSpot'>

                    </div>
                    <div id='scheduleTableSpot'>

                    </div>
                    
                    <p id='testing'></p>
                
                
                </div>
            ";
        }

        /* 
        * User is a regular user, no adding/deleting games/etc.
        */
        elseif($userType == 2)
        {
            echo 
            "
                <div class='container'>
                    <div class='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom'>
                        <h1 class='h2'>User Dashboard</h1>
                        <div class='btn-toolbar mb-2 mb-md-0'>
                            <button class='btn btn-warning' type='button' data-toggle='modal' data-target='#helpModal'>Help</button>
                            <a href='logout.php'><button class='btn btn-danger ml-2' type='button'>Logout</button></a>
            
                            <!-- Help Modal -->
                            <div class='modal fade' id='helpModal' tabindex='-1' role='dialog' aria-labelledby='helpModal' aria-hidden='true'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header'>
                                            <h5 class='modal-title' id='exampleModalLabel'>Dashboard Help</h5>
                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                                <span aria-hidden='true'>&times;</span>
                                            </button>
                                        </div>
                                        <div class='modal-body'>
                                            <h1 class='h3'>How to Become a Coach/Admin</h1>
                                            <p class='text-muted'>
                                                To become a <b>COACH</b> or <b>ADMIN</b> for the site, <a href='mailto:example@gmail.com'>contact an administrator</a> and they can grant you permission to become one.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class='container'>
                        To become a <b>COACH</b> or <b>ADMIN</b> for the site, <a href='mailto:example@gmail.com'>contact an administrator</a> and they can grant you permission to become one.
                    </div>
                </div>
            ";
        }
    ?>

    <script src="js/functions.js"></script>
    <script>
        // Should match date format yyyy-mm-dd
        var dateRegex = /^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/;

        var date = getISODateWithoutTime(); // yyyy-mm-dd
        
        // DOM variables
        var dateInput = document.getElementById('date-input');
        var opponent = document.getElementById('opponent-input');
        var ourScore = document.getElementById('score-input');
        var oppScore = document.getElementById('opp-score-input');

        // Valdates form inputs before submitting addgame.php form
        function validateForm(){

            // Validate date 
            if(!dateInput.value.match(dateRegex)) {
                alert("Wrong date format");
                return false;
            }

            // Validate opponent name
            if(opponent.value.length < 3){
                alert("Opponent name must be 3 characters or more in length");
                return false;
            }

            // Validate scores
            if((ourScore.value && !oppScore.value) || (!ourScore.value && oppScore.value)){
                alert("Put both scores for game");
                return false;
            }

            // Validate scores
            if(ourScore.value < 0 || oppScore.value < 0) {
                alert("Game can't have negative scores!");
                return false;
            }

            // Ensure scores aren't input for future game
            if((ourScore.value && oppScore.value) && date < dateInput.value){
                alert("Can't input scores for a future game");
                return false;
            }
        }

        function validateGlobalForm()
        {
            console.log("validating global form!");
            var team = document.getElementById("team-input");
            
            // Validate date 
            if(!dateInput.value.match(dateRegex)) {
                alert("Wrong date format");
                return false;
            }

            // Validate team name
            if(opponent.value.length < 3 || team.value.length < 3)
            {
                alert("Team names must be 3 characters or more");
                return false;
            }

            // Validate scores
            if((ourScore.value && !oppScore.value) || (!ourScore.value && oppScore.value)){
                alert("Put both scores for game");
                return false;
            }

            // Validate scores
            if(ourScore.value < 0 || oppScore.value < 0) {
                alert("Game can't have negative scores!");
                return false;
            }

            // Ensure scores aren't input for future game
            if((ourScore.value && oppScore.value) && date < dateInput.value){
                alert("Can't input scores for a future game");
                return false;
            }

        }

        // Loads schedule on button click
        function loadScheduleSeasonSelector()
        {
            // Coach
            if(<?php echo $userType ?> == 0)
            {
                var teamInput = "<?php echo $team_name; ?>";
                teamInput = $.trim(teamInput);
            }
            // Admin
            else
            {
                var teamInput = document.getElementById("schedule-search-team-input").value; // string of team name from input
            } 

            console.log(teamInput);

            $.ajax({
                type: "get",
                url: "do-games-exist.php?teamname=" + teamInput,
                success: function (data) {
                    var a = data;
                    if(a == "false")
                    {
                        $("#selectSeasonSpot").html("<p>No team found with that name or no games to show.</p>");
                        $("#scheduleTableSpot").html("");
                    }
                    else
                    {
                        $("#selectSeasonSpot").html(a);
                        $("#viewButton").click();
                    }
                }
            });
        }

        // on `view` button click Displays table below `select` 
        function updateScheduleYear()
        {
            // console.log("updateScheduleYear() clicked!...\n");
            var a = $("#selectSeason option:selected").text();
            var year = a.substring(0, 4);

            // Coach
            if(<?php echo $userType ?> == 0)
            {
                var teamName = "<?php echo $team_name; ?>";
                teamName = $.trim(teamName);
            }
            // Admin
            else
            {
                var teamName = $("#schedule-search-team-input").val();
            } 

            $.ajax({
                type: "get",
                url: "get-team-schedule-season.php?teamname=" + teamName + "&season=" + year,
                success: function (data) {
                    // console.log(data);
                    $("#scheduleTableSpot").html(data);
                }
            });
        }

        // Deletes row from schedule and submits AJAX request to delete game
        function deleteRow(id)
        {
            console.log("Delete row");
            var rowID = "gameRow" + id;
            console.log(rowID);

            // Deletes row physically
            var row = document.getElementById(rowID);
            row.parentNode.removeChild(row);

            // Delete row from database
            if(window.XMLHttpRequest)
            {
                // Code for IE7+, FireFox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else
            {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }

            xmlhttp.open("POST","deletegame.php?id="+id,true);
            xmlhttp.send();
        }

        // Helper function to call loadScheduleSeasonSelector() function on coach load 
        $(function(){
            $('div[coachTeamScheduleViewer]').trigger('onload');
            
            // If user is coach, call function
            if(<?php echo $userType;?> == 0)
            {
                loadScheduleSeasonSelector();
            }
        });

        // On window resize
        $(window).resize(function(){
            // if coach
            if(<?php echo $userType; ?> == 0)
            {
                checkPositionCoach();
            }
            else if(<?php echo $userType; ?> == 1)
            {
                checkPositionAdmin();
            }
        });

        // On page load
        $( document ).ready(function() {

            // Make link active
            $("#dashboardLink").addClass("active");

            // if coach
            if(<?php echo $userType; ?> == 0)
            {
                checkPositionCoach();
            }
            else if(<?php echo $userType; ?> == 1)
            {
                checkPositionAdmin();
            }
        });

        // Media Queries
        function checkPositionCoach() {
            // 850 px or below
            if (window.matchMedia('(max-width: 850px)').matches) {
                // Make inputs on distinct lines
                $(".addGameRow").removeClass("row");
                $(".addGameRow").removeClass("form-group");

                // Position button correctly
                $(".submitDiv").addClass("mt-3");
                $(".submitDiv").addClass("col");
            }
            else
            {
                // Make inputs on same lines
                $(".addGameRow").addClass("row");
                $(".addGameRow").addClass("form-group");

                // Position button correctly
                $(".submitDiv").removeClass("mt-3");
                $(".submitDiv").removeClass("col");
            }

            // 500px or below
            if(window.matchMedia('(max-width: 500px').matches) {
                // Center dashboard title text
                $(".dashboardTitle").addClass("text-center");
                $(".mainFormatDiv").removeClass("d-flex");
            }
            else
            {
                // Un-center dashboard title text
                $(".dashboardTitle").removeClass("text-center");
                $(".mainFormatDiv").addClass("d-flex");
            }
        }

        // Admin media queries
        function checkPositionAdmin() {
            // 500px or below
            if(window.matchMedia('(max-width: 500px').matches) {
                $(".addGameRow").removeClass("row");
                $(".addGameRow").removeClass("form-group");

                // Position button correctly
                $(".submitDiv").addClass("mt-3");
                $(".submitDiv").addClass("col");
            }
            else
            {
                $(".addGameRow").addClass("row");
                $(".addGameRow").addClass("form-group");

                // Position button correctly
                $(".submitDiv").removeClass("mt-3");
                $(".submitDiv").removeClass("col");
            }
        }   
        
                
    </script>


    <!-- Allows for live search dropdown-->
    <script src="js/live-search.js"></script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>