<!-- AJAX returns schedule in table format --> 
<?php require_once 'connect.php'; ?>
<?php include('functions.php')?>
<?php

$teamname = $_GET['teamname'];

// Get team ID of teamname
$sql = "SELECT Team_id from team WHERE Team_name='$teamname' LIMIT 1";
$result = mysqli_query($conn, $sql);
$teamIDArray = mysqli_fetch_all($result, MYSQLI_ASSOC);

// If no team was found
if(sizeof($teamIDArray) == 0)
{
    echo
    "
        <p>No team found with that name</p>
    ";
    return;
}

$team_id = $teamIDArray[0]['Team_id']; // Holds team ID of selected team

// Find any game involving the team and order by date
$sql = "SELECT * FROM game WHERE Game_team1='$team_id' or Game_team2='$team_id' ORDER BY Game_date";
$result = mysqli_query($conn, $sql); // Stores all table data in query
$games = mysqli_fetch_all($result, MYSQLI_ASSOC); // Holds array of games played by dashboard team

$gameCount = count($games); // Number of games played in total

// If no games in schedule
if($gameCount == 0)
{
    echo
    "
        <p>Team has no games in schedule</p>
    ";
    return;
}



echo 
"
<!-- Start of table -->
<table class='table table-bordered mb-5 mt-3'>
    <thead class='thead border-bottom'>
        <tr>
            <th scope='col'>Date</th>
            <th scope='col'>Opponent</th>
            <th scope='col'>Score</th>
            <th scope='col'>Action</th>
        </tr>
    </thead>
    <tbody>
    <!-- Fill table with games -->
";


// Tracking record
$winCountOverall = 0;
$lossCountOverall = 0;


// Output table
for ($i = 0; $i < $gameCount; $i++) {

    // Sets date of game in schedule format
    $gameDate = formatDateForSchedule($games[$i]['Game_date']);

    $winOrLoss = ''; // Holds 'W' or 'L'

    // Holds teams ID's
    $team1 = $games[$i]['Game_team1'];
    $team2 = $games[$i]['Game_team2'];

    // Holds team scores
    $team1Score = $games[$i]['Game_team1score'];
    $team2Score = $games[$i]['Game_team2score'];

    $gameID = $games[$i]['Game_id'];

    // Holds 1 or 2 depending on which team is shown
    $team1or2 = thisTeam($team1, $team2, $team_id);

    // Final score
    $score = '';

    // Set opponent ID
    if ($team1or2 == 1) {
        $opponentID = $team2; // Holds Team_id of opposing team
    } else {
        $opponentID = $team1; // Holds Team_id of opposing team
    }

    // If opponent isn't in database
    if ($opponentID == '') {
        $opponent = $games[$i]['Game_oppName'];
    }
    // If opponent is in database
    else {
        $opponentSQL = "SELECT Team_name FROM team WHERE Team_id='$opponentID'";
        $opponentResult = mysqli_query($conn, $opponentSQL);
        $OpponentArray = mysqli_fetch_all($opponentResult, MYSQLI_ASSOC);
        $opponent = $OpponentArray[0]['Team_name']; // Holds opponent name 
    }

    // If game has been played
    if (isGamePlayed($team1Score, $team2Score)) {
        $score = getScore($team1Score, $team2Score, $team1or2, $winOrLoss); // In format 'W 49-40'
    }

    // If score isn't empty
    if ($score != '') {
        if ($score[0] == 'W') {
            $winCountOverall++;
        } else if ($score[0] == 'T') {
        } else {
            $lossCountOverall++;
        }
    }

    // Outputs HTML row data
    scheduleOutputRowDashboard($gameDate, $opponentID, $opponent, $score, $gameID);
}

echo 
"
    </tbody>
</table>
";


mysqli_close($conn);
?>