<?php require_once 'connect.php'; ?>
<?php

// if url has ?id=xx
if (isset($_GET['id'])) {

  // Holds id number of the team
  $id = mysqli_real_escape_string($conn, $_GET['id']);

  // If url has &season=xxxx
  if (isset($_GET['season'])) {

    $seasonNum = mysqli_real_escape_string($conn, $_GET['season']); // Holds value of current shown season
    $sql = "SELECT * from game WHERE (Game_team1='$id' or Game_team2='$id') AND Game_season='$seasonNum' ORDER BY Game_date";
  } else {

    // Display current season 
    $month = date("m");
    $year = date("Y");
    if ($month < 7) {
      $seasonNum = $year - 1;
    } else {
      $seasonNum = $year;
    }

    $sql = "SELECT * FROM game WHERE (Game_team1='$id' or Game_team2='$id') AND Game_season='$seasonNum' ORDER BY Game_date";
  }

  $result = mysqli_query($conn, $sql); // Stores all table data in query
  $games = mysqli_fetch_all($result, MYSQLI_ASSOC); // Holds array of games

  $gameCount = count($games); // Number of games played

  $sql2 = "SELECT Team_name, Team_league, Team_mascot, Team_address, Team_city, Team_state, Team_colors FROM team WHERE Team_id='$id'";
  $teamNameResult = mysqli_query($conn, $sql2);
  $teamNameArray = mysqli_fetch_all($teamNameResult, MYSQLI_ASSOC);

  $teamName = $teamNameArray[0]['Team_name'];
  $leagueName = $teamNameArray[0]['Team_league'];
  $teamMascot = $teamNameArray[0]['Team_mascot'];
  $teamAddress = $teamNameArray[0]['Team_address'];
  $teamCity = $teamNameArray[0]['Team_city'];
  $teamState = $teamNameArray[0]['Team_state'];
  $teamColors = $teamNameArray[0]['Team_colors'];
}

// Else redirect back to home
else {
  header('Location: index.php');
}

?>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <!-- Font CSS -->
  <link href="https://fonts.googleapis.com/css?family=Alatsi&display=swap" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/styles.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>

  <title><?php echo "$teamName $teamMascot Schedule" ?></title>

</head>

<body>

  <!-- Navbar -->
  <?php include('navbar.php'); ?>

  <!-- Team information and schedule -->
  <div class="container mt-2">

    <!-- Team Information -->
    <div class="row mb-2">
      <div class="col-3 my-auto">
        <img src="img/teamlogo/<?php echo "$id"; ?>.jpg" width="100%" height="100%">
      </div>
      <div class="col-9 my-auto">
        <h1 class="mb-0 h3"><?php echo "$teamName $teamMascot" ?></h1>
        <?php

        // Get this season's wins, losses, wlratio
        $seasonRecordSQL = "SELECT wins, losses, wlratio, leagueWins, leagueLosses, leagueWLRatio FROM season WHERE season=$seasonNum AND team_id=$id";
        $seasonRecordResult = mysqli_query($conn, $seasonRecordSQL);
        $seasonRecordArray = mysqli_fetch_all($seasonRecordResult, MYSQLI_ASSOC);
        $seasonRecordArrayCt = count($seasonRecordArray); // 1 if season exists, 0 if not

        // Default initialization of record info
        $teamWins = $teamLosses = $teamWLRatio = $teamLeagueWins = $teamLeagueLosses = $teamLeagueWLRatio = 0;

        // If season record info exists
        if ($seasonRecordArrayCt == 1) {
          // Holds team information
          $teamWins = $seasonRecordArray[0]['wins'];
          $teamLosses = $seasonRecordArray[0]['losses'];
          $teamWLRatio = $seasonRecordArray[0]['wlratio'];
          $teamLeagueWins = $seasonRecordArray[0]['leagueWins'];
          $teamLeagueLosses = $seasonRecordArray[0]['leagueLosses'];
          $teamLeagueWLRatio = $seasonRecordArray[0]['leagueWLRatio'];
        }

        ?>
        <p class="mb-0"><small id="overallRecord"><span class="text-muted">Overall Record: </span><?php echo "$teamWins-$teamLosses"; ?></small></p>
        <p class="mb-0"><small id="league"><span class="text-muted">League: </span></small></p>
        <p class="mb-0"><small><span class="text-muted">League Record: </span><?php echo "$teamLeagueWins-$teamLeagueLosses"; ?></small></p>
      </div>
    </div>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="schedule-tab" data-toggle="tab" href="#schedule" role="tab" aria-controls="schedule" aria-selected="true">Schedule</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="team-info-tab" data-toggle="tab" href="#team-info" role="tab" aria-controls="profile" aria-selected="false">Team Info</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="schedule" role="tabpanel" aria-labelledby="schedule-tab">

        <!-- Select Season year -->
        <div class="row">
          <div class="col-sm">
            <div class="input-group mt-3">
              <select class="custom-select" id="selectSeason">
                <?php
                // Find any game involving the team and order by date
                $seasonSql = "SELECT DISTINCT Game_season FROM game WHERE Game_team1='$id' or Game_team2='$id' ORDER BY Game_season DESC";
                $seasonResults = mysqli_query($conn, $seasonSql); // Stores all table data in query
                $seasons = mysqli_fetch_all($seasonResults, MYSQLI_ASSOC); // Holds array of games

                $seasonCt = count($seasons); // Holds number of seasons

                for ($i = 0; $i < $seasonCt; $i++) {
                  echo "<option value='" . $seasons[$i]['Game_season'] . "'>" . $seasons[$i]['Game_season'] . "-" . ($seasons[$i]['Game_season'] + 1) . "</option>";
                }

                // if current season is set, select that option
                if (!isset($_GET['season'])) {
                  echo
                    '
                    <script>
                      // Select current season option
                      var seasonNum = document.getElementById("selectSeason").value="' . $seasonNum . '";
                      seasonNum.selected = true;
                    </script>
                  ';
                }
                // if season is set
                else {
                  echo
                    '
                    <script>
                      // Select current season option
                      var seasonOption = document.getElementById("selectSeason").value="' . $seasonNum . '";
                      seasonOption.selected = true;
                    </script>
                  ';
                }
                ?>
              </select>
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" onclick="updateScheduleYear()">View</button>
              </div>
            </div>
          </div>
          <div class="col"></div>
          <div class="col"></div>
        </div>

        <!-- Schedule Table -->
        <div class="mt-3">
          <table class="table table-bordered mb-5">
            <thead class="thead border-bottom">
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Opponent</th>
                <th scope="col">Score</th>
              </tr>
            </thead>
            <tbody>
              <?php include 'functions.php' ?>
              <!-- Fill table with games -->
              <?php

              // Iterate through all games that this team has played
              for ($i = 0; $i < $gameCount; $i++) {

                // Sets date of game in schedule format
                $gameDate = formatDateForSchedule($games[$i]['Game_date']);

                $winOrLoss = ""; // Holds 'W' or 'L'

                // Holds teams ID's
                $team1 = $games[$i]['Game_team1'];
                $team2 = $games[$i]['Game_team2'];

                // Holds team scores
                $team1Score = $games[$i]['Game_team1score'];
                $team2Score = $games[$i]['Game_team2score'];

                // Holds 1 or 2 depending on which team is shown
                $team1or2 = thisTeam($team1, $team2, $id);

                // Final score
                $score = "";

                // Set opponent ID
                if ($team1or2 == 1) {
                  $opponentID = $games[$i]['Game_team2']; // Holds Team_id of opposing team
                } else {
                  $opponentID = $games[$i]['Game_team1'];
                }

                // If opponent isn't in database
                if ($opponentID == "") {
                  $opponent = $games[$i]['Game_oppName'];
                }
                // If opponent is in database
                else {
                  $opponentSQL = "SELECT Team_name FROM team WHERE Team_id='$opponentID'";
                  $opponentResult = mysqli_query($conn, $opponentSQL);
                  $OpponentArray = mysqli_fetch_all($opponentResult, MYSQLI_ASSOC);
                  $opponent = $OpponentArray[0]['Team_name']; // Holds opponent name 
                }

                // If game has been played
                if (isGamePlayed($team1Score, $team2Score)) {
                  $score = getScore($team1Score, $team2Score, $team1or2, $winOrLoss); // In format 'W 49-40'
                }

                scheduleOutputRow($gameDate, $opponentID, $opponent, $score);
              }

              // Display overall record and league
              echo
                "
                    <script>
                      // Display link to league and name
                      var league = document.getElementById('league');
                      league.innerHTML += '<a class=\'custom-link ai-element ai-element_type2 ai-element3\' href=\'./league.php?league=$leagueName\'>$leagueName</a>';
                    </script>
                    ";

              ?>
            </tbody>
          </table>
        </div>

      </div>
      <!-- Team Info Tab --> 
      <div class="tab-pane fade" id="team-info" role="tabpanel" aria-labelledby="team-info-tab">
            <table class="table table-hover mt-4">
              <tbody>
                <tr>
                  <th scope="row">Address</th>
                  <td><?php echo $teamAddress; ?></td>
                </tr>
                <tr>
                  <th scope="row">City</th>
                  <td><?php echo "$teamCity, $teamState"?></td>
                </tr>
                <tr>
                  <th scope="row">Colors</th>
                  <td><?php echo $teamColors ?></td>
                </tr>

                <?php 
                  
                  // Tracker variables for points in season
                  $points = 0;
                  $oppPoints = 0;
                  $gameCount = 0;

                  
                  // Go through each PLAYED this season
                  foreach($games as $game){
                    // if game was played
                    if($game['Game_team1score'] != ""){
                      $gameCount++; // Add a game as played
                      
                      // if this team is team1
                      if($game['Game_team1'] == $id){
                        $points += $game['Game_team1score']; // Add points for this team
                        $oppPoints += $game['Game_team2score']; // Add points for opposing team
                      }
                      else {
                        $points += $game['Game_team2score']; // Add points for this team
                        $oppPoints += $game['Game_team1score']; // Add points for opposing team
                      }
                    }
                  }
                  
                  if($points == 0)
                  {
                    $gameCount = 1; // Avoid division by 0 error
                  }

                  $PPG = round($points/$gameCount, 2);
                  $oppPPG = round($oppPoints/$gameCount, 2);
                
                ?>


                <tr>
                  <th scope="row">Season Win %</th>
                  <td><?php echo round(($teamWLRatio * 100), 2) . "%"; ?></td>
                </tr>
                <tr>
                  <th scope="row">Season PPG</th>
                  <td><?php echo $PPG; ?></td>
                </tr>
                <tr>
                  <th scope="row">Season Opponent PPG</th>
                  <td><?php echo $oppPPG; ?></td>
                </tr>
              </tbody>
            </table>
      </div>
    </div>

  </div>

  <script src="js/functions.js"></script>
  <script>
    // Update schedule year link
    function updateScheduleYear() {
      var selectSeasonDropdown = document.getElementById("selectSeason");
      var seasonSelected = selectSeasonDropdown.options[selectSeasonDropdown.selectedIndex].value; // Value of selected season

      // Get new URL and redirect
      var newURL = updateURLParameter(window.location.href, 'season', seasonSelected);
      window.location.href = newURL;
    }
  </script>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>