# Ohio Basketball

Website for Ohio High School Basketball. Contains schedules, scores, rosters, team information. The better version of MaxPreps

## How to Setup Locally
Built using **PHP and MySQL**

1. Setup [PHP & MySQL](https://www.apachefriends.org/download.html)
2. Create database *mvac_db* on localhost/phpmyadmin
3. Import *prjdb.sql* into new database
4. Change $conn variables in *connect.php* to connect to database

## Useful Tools
1. [Mockaroo](https://mockaroo.com/) - Random SQL statement generator (good for filling up tables with data)
2. [PHPUnit](https://phpunit.readthedocs.io/en/8.5/) - Allows for PHP testing. 
    - To install, run *composer require phpunit/phpunit*
    -  Tests are in the *tests/* folder
    -  *composer.json* contains the autoload information
    -  *phpunit.xml* has configuration settings
    -  To run, execute *phpunit* in cmd
3. [Regexr](https://regexr.com/) - Useful for allowing RegExp testing