// Todo: Change available tags to teams in database
$(function () {
  $.get("get-database-teams.php", function (data) {
    var availableTags = JSON.parse(data);

    /*var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme",
      ];*/
      
      $(".team-input").autocomplete({
        source: availableTags,
      });
  });
});
