<!-- AJAX returns users in table format -->
<?php require_once 'connect.php'; ?>
<?php include('functions.php') ?>
<?php
$username = "";
$val = "";

if(isset($_GET["username"])) {
    $username = $_GET["username"]; // Gets username
    $val = $_GET["val"]; // Either 'add' or 'remove'
}

// Get user row
$sql = "SELECT userType FROM users WHERE username='$username'";
$result = mysqli_fetch_row(mysqli_query($conn, $sql));

// Type of user found
$userType = $result[0];

$unknownError = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error</strong> There was a problem with your request.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

// Errors
if ($userType == "" || $userType == "1" || $val == "") {

    // No user found
    if ($userType == "") {
        echo "<p class='text-danger'>No user was found with that username</p>";
    } elseif ($userType == "1") { // User is admin
        echo "<p class='text-danger'>User is already an admin, which has more capabilities than a coach.</p>";
    } else {
        echo $unknownError;
    }
} else {
    // Updates user to a coach
    if ($val == "add") {
        $update = "UPDATE users SET userType=0 WHERE username='$username'";
        if (mysqli_query($conn, $update)) {
            echo
                '
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> User has been upgraded to a coach.
        </div>
        ';
        } else {
            echo $unknownError;
        }
    } elseif ($val == "remove") {
        $update = "UPDATE users SET userType = 2 WHERE username='$username'";

        if (mysqli_query($conn, $update)) {
            echo
                '
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> User has been downgraded from a coach to a normal user.
        </div>
        ';
        }
    } else {
        echo $unknownError;
    }
}


?>