<?php require_once 'connect.php'; ?>
<?php include('functions.php')?>
<?php 
    $sql = "SELECT Team_name FROM team WHERE TRUE";
    $results = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

    $final = [];

    // Fill array
    for($i = 0; $i < sizeof($results); $i++) {
        array_push($final, $results[$i]['Team_name']);
    }

    echo json_encode($final); // Return array
?>