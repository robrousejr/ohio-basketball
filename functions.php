<?php 

    // Returns bool if game has been played
    function isGamePlayed($score1, $score2) {
        if($score1 == NULL || $score2 == NULL) {
            return false;
        }
        else {
            return true;
        }
    }

    // Returns if viewed team is team 1 or team 2
    function thisTeam($team1, $team2, $id)  {
        if($team1 == $id) {
            return 1;
        }
        else {
            return 2;
        }
    }

    // Returns string of final score
    function getScore($team1Score, $team2Score, $thisTeam, $winOrLoss) {
        
        if($thisTeam == 1) {
            if($team1Score > $team2Score) {
                $winOrLoss = "W";
            }
            else if($team1Score == $team2Score) {
                $winOrLoss = "Tie";
            }
            else {
                $winOrLoss = "L";
            }

            return "$winOrLoss $team1Score-$team2Score";
        }
        else { 
            if($team2Score > $team1Score) {
                $winOrLoss = "W";
            }
            else if($team2Score == $team1Score) {
                $winOrLoss = "Tie";
            }
            else {
                $winOrLoss = "L";
            }
            
            return "$winOrLoss $team2Score-$team1Score";
        }
    }

    // Returns proper date string for schedule output 
    function formatDateForSchedule($sqlDate) {
        $sqlDateTime = DateTime::createFromFormat('Y-m-d', $sqlDate);
        $gameDate = date_format($sqlDateTime, "m/d/y");

        return $gameDate;
    }

    // Returns proper date string minified ---> m/d
    function formatDateForScheduleMinified($sqlDate){
        $sqlDateTime = DateTime::createFromFormat('Y-m-d', $sqlDate);
        $gameDate = date_format($sqlDateTime, "m/d");

        return $gameDate;
    }


    // Returns proper date for table update or insert
    function formatDateForTable($sqlDate) {
        $sqlDateTime = DateTime::createFromFormat('m/d/y', $sqlDate);
        $gameDate = date_format($sqlDateTime, "Y-m-d");

        return $gameDate;
    }


    // Returns boolean for if opponent is in database
    function isOpponentExists($opponentID, $conn) {
        
        // Find if opponent ID number exists
        $sql = "SELECT * FROM team WHERE Team_id LIKE '$opponentID' LIMIT 1"; // Very fast way to check for text
        $result = mysqli_query($conn, $sql); // Stores all table data in query
        $teams = mysqli_fetch_all($result, MYSQLI_ASSOC); // Holds array of team

        // Number of teams returned
        $ct = count($teams);

        if($ct == 1) {
            return true;
        }
        else {
            return false;
        }
        
    }

    // Echoes row values for team schedules
    function scheduleOutputRow($gameDate, $opponentID, $opponent, $score){

        echo  "<tr><td scope='row'>$gameDate</td>";

        // If opponent is unknown
        if($opponentID != "") {
            echo "<td scope='row'><a class='custom-link ai-element ai-element_type2 ai-element3' href='./team.php?id=$opponentID'>$opponent</a></td>";
        }
        // Opponent is in database
        else {
            echo "<td scope='row'>$opponent</td>";
        }

        echo "<td class='game-score' scope='row'>$score</td></tr>";
    }

    // Echoes row values for dashboard schedule
    function scheduleOutputRowDashboard($gameDate, $opponentID, $opponent, $score, $gameID){
        echo  "<tr id='gameRow$gameID'><td scope='row'>$gameDate</td>";

        // If opponent is unknown
        if($opponentID != "") {
            echo "<td scope='row'><a a class='custom-link ai-element ai-element_type2 ai-element3' href='./team.php?id=$opponentID'>$opponent</a></td>";
        }
        // Opponent is in database
        else {
            echo "<td scope='row'>$opponent</td>";
        }

        echo "<td scope='row'>$score</td>";
        echo "<td scope='row'>
        <a class='btn btn-danger' style='color:white;' role='button' onclick='deleteRow($gameID)'>Delete</a>
        </td>";
    }
