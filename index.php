<?php require_once 'connect.php'; ?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <!-- Font CSS -->
  <link href="https://fonts.googleapis.com/css?family=Alatsi&display=swap" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/styles.css" rel="stylesheet" type="text/css">
  <link href="css/upcoming-games.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" type="image/png" href="img/favicon.png" />

  <title>MVAC Basketball</title>
</head>

<body>

  <!-- Navbar -->
  <?php include('navbar.php'); ?>

  <!-- Top Section Carousel -->
  <div class="container mt-5 mb-5">
    <div id="carouselExampleIndicators" class="carousel slide top-index-carousel" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="img/carousel1.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/carousel2.jpg" alt="Second slide">
          <div class="carousel-caption">
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/carousel3.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>

  <!-- Display upcoming games -->
  <div class="container" id="upcomingGamesDiv">

  </div>


  <!-- Chatroom Tab -->
  <div class="container" id="profile">
    <h1 class="text-center">MVAC Chatroom</h1>
    <iframe class="mt-3 mb-5" src="https://minnit.chat/MVAC?embed&nickname=" style="border:none;width:100%;height:500px;" allowTransparency="true"></iframe><br><a href="https://minnit.chat/MVAC" target="_blank"></a>
  </div>

  <?php include 'footer.php'; ?>

  <!-- Popper, then Bootstrap -->
  <script src="js/jquery-3.5.0.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  <script>
    // Interval carousel
    $('.carousel').carousel({
      interval: 2000
    })

    // On page load
    $(document).ready(function() {
      $('.carousel').carousel() // Call carousel 
      loadUpcomingGames();
    });

    // On page resize
    $(window).on('resize', function(){
      loadUpcomingGames();
    });

    // Loads and inserts upcoming games
    function loadUpcomingGames() {
      // If width is less than 767px
      if (window.matchMedia('(max-width: 767px)').matches) {
        // Ajax call ouptputs upcoming games
        $.ajax({
          type: "get",
          url: "upcoming-games.php?type=mobile",
          success: function(data) {
            $("#upcomingGamesDiv").html(data); // Insert html
          }
        });
      } else {
        // Ajax call ouptputs upcoming games
        $.ajax({
          type: "get",
          url: "upcoming-games.php?type=normal",
          success: function(data) {
            $("#upcomingGamesDiv").html(data); // Insert html
          }
        });
      }
    }

  </script>
</body>

</html>