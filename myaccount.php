<?php require_once 'connect.php'; ?>
<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

// Define variables and initialize with empty values
$new_password = $confirm_password = "";
$new_password_err = $confirm_password_err = "";

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Validate new password
    if (empty(trim($_POST["new_password"]))) {
        $new_password_err = "Please enter the new password.";
    } elseif (strlen(trim($_POST["new_password"])) < 6) {
        $new_password_err = "Password must have atleast 6 characters.";
    } else {
        $new_password = trim($_POST["new_password"]);
    }

    // Validate confirm password
    if (empty(trim($_POST["confirm_password"]))) {
        $confirm_password_err = "Please confirm the password.";
    } else {
        $confirm_password = trim($_POST["confirm_password"]);
        if (empty($new_password_err) && ($new_password != $confirm_password)) {
            $confirm_password_err = "Password did not match.";
        }
    }

    // Check input errors before updating the database
    if (empty($new_password_err) && empty($confirm_password_err)) {
        // Prepare an update statement
        $sql = "UPDATE users SET password = ? WHERE id = ?";

        if ($stmt = mysqli_prepare($conn, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "si", $param_password, $param_id);

            // Set parameters
            $param_password = password_hash($new_password, PASSWORD_DEFAULT);
            $param_id = $_SESSION["id"];

            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Password updated successfully. Destroy the session, and redirect to login page
                session_destroy();
                header("location: login.php");
                exit();
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($conn);
}

$param_id = $_SESSION["id"]; // Contains user ID of user
$userSQL = "SELECT userType FROM users WHERE id=$param_id LIMIT 1";
$userArray = mysqli_fetch_row(mysqli_query($conn, $userSQL));
$userType = $userArray[0]; // Contains type of user
?>


<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Alatsi&display=swap" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/styles.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />

    <title>MVAC Basketball</title>
</head>

<body>

    <!-- Navbar -->
    <?php include('navbar.php'); ?>

    <div class="container mt-5 mb-5">
        <h2>Reset Password</h2>
        <p class="text-muted">Please fill out this form to reset your password.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($new_password_err)) ? 'has-error' : ''; ?>">
                <label>New Password</label>
                <input type="password" name="new_password" class="form-control" value="<?php echo $new_password; ?>">
                <small class="text-muted"><?php echo $new_password_err; ?></small>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control">
                <small class="text-muted"><?php echo $confirm_password_err; ?></small>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </div>

    <hr>

    <!-- USER ADD/REMOVE BY ADMIN -->
    <?php
    // If Admin
    if ($userType == 1) {

        // Todo: Implement input bar and submit button. Also make slider for "Add as Coach/Remove as Coach"
        // Todo: Add validation
        echo '
        <div class="container mt-5">
            <h2>Add/Remove Coaches</h2>
            <form id="addCoachForm">
                <div class="form-group mt-4">
                    <input type="text" class="form-control" placeholder="Username..." id="username">
                </div>
                <div class="form-group">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" checked class="custom-control-input" id="customSwitch1">
                        <label class="custom-control-label" for="customSwitch1">Add as Coach</label>
                    </div>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch2">
                        <label class="custom-control-label" for="customSwitch2">Remove as Coach</label>
                    </div>
                </div>
                <div class="form-group">
                    <input id="coachSubmit" type="submit" class="btn btn-primary" value="Submit">
                </div>
            </form>

            <div id="outputUsersTable" class="mt-4">
                <!-- Where the output will be put -->
            </div>
        </div>
        ';
    }
    ?>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.5.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {
            $("#myaccountLink").addClass("active");
        });

        // Stop form from reloading page
        $("#addCoachForm").submit(function(e) {
            e.preventDefault();
        });

        // If add coach is toggled
        $("#customSwitch1").click(function() {
            // Make other false if this one is true
            if ($("#customSwitch1").prop("checked") == true) {
                $("#customSwitch2").prop("checked", false); // Set other to false
            }
        });

        // If Remove coach is toggled
        $("#customSwitch2").click(function() {
            // Make other false if this one is true
            if ($("#customSwitch2").prop("checked") == true) {
                $("#customSwitch1").prop("checked", false); // Set other to false
            }
        });

        // On submit button click for addcoach
        $("#coachSubmit").click(function() {

            var username = $("#username").val();
            var addCoach = "";

            // Verifying at least one is toggled 
            if ($("#customSwitch1").prop("checked") == true || $("#customSwitch2").prop("checked") == true) {
                if ($("#customSwitch1").prop("checked") == true) {
                    addCoach = "add";
                } else {
                    addCoach = "remove";
                }
            }

            // AJAX function
            $.ajax({
                type: "get",
                url: "get-user.php?username=" + username + "&val=" + addCoach,
                success: function(data) {
                    $("#outputUsersTable").html(data); // Output table
                }
            });
        });
    </script>
</body>

</html>