<?php

// Start session if session hasn't been started
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 


echo 
'
  <nav class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar bg-primary">
    <a class="navbar-brand" href="index.php">MVAC Basketball</a>

    <!-- Should be good for mobile --> 
    <div class="navbar-nav-scroll">
      <ul class="navbar-nav bd-navbar-nav flex-row">

        <li class="nav-item dropdown">
          <!-- Team Dropdown -->  
          <a class="nav-link dropdown-toggle" id="teamDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Teams</a>
          <div class="dropdown-menu" aria-labelledby="teamDropdown">
            <a class="dropdown-item" href="team.php?id=1">Jackson Milton</a>
            <a class="dropdown-item" href="team.php?id=2">Lowellville</a>
            <a class="dropdown-item" href="team.php?id=3">McDonald</a>
            <a class="dropdown-item" href="team.php?id=4">Mineral Ridge</a>
            <a class="dropdown-item" href="team.php?id=5">Sebring</a>
            <a class="dropdown-item" href="team.php?id=6">Springfield</a>
            <a class="dropdown-item" href="team.php?id=7">Waterloo</a>
            <a class="dropdown-item" href="team.php?id=8">Western Reserve</a>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link" id="dashboardLink" href="dashboard.php">Dashboard</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" id="myaccountLink" href="myaccount.php">My Account</a>
        </li>

      </ul>
    </div>
  </nav>
';
?>