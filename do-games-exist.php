<?php require_once 'connect.php'; ?>
<?php include('functions.php')?>
<?php

$teamname = $_GET['teamname'];

// Get team ID of teamname
$sql = "SELECT Team_id from team WHERE Team_name='$teamname' LIMIT 1";
$teamIDArray = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC);

// If no team was found
if(sizeof($teamIDArray) == 0)
{
    echo "false";
    return;
}

$team_id = $teamIDArray[0]['Team_id']; // Holds team ID of selected team

// Find any game involving the team and order by date
$sql = "SELECT * FROM game WHERE Game_team1='$team_id' or Game_team2='$team_id' LIMIT 1";
$games = mysqli_fetch_all(mysqli_query($conn, $sql), MYSQLI_ASSOC); // Holds array of games played by dashboard team

if(count($games) == 0)
{
    echo "false";
    return;
}
?>
<!-- Select Season year -->
<div class="row">
    <div class="col-sm">
    <div class="input-group mt-3">
        <select class="custom-select" id="selectSeason">
        <?php
        // Find any game involving the team and order by date
        $seasonSql = "SELECT DISTINCT Game_season FROM game WHERE Game_team1='$team_id' or Game_team2='$team_id' ORDER BY Game_season DESC";
        $seasonResults = mysqli_query($conn, $seasonSql); // Stores all table data in query
        $seasons = mysqli_fetch_all($seasonResults, MYSQLI_ASSOC); // Holds array of games
        $latestSeason = intval($seasons[0]["Game_season"]);

        $seasonCt = count($seasons); // Holds number of seasons

        for ($i = 0; $i < $seasonCt; $i++) {
            echo "<option value='" . $seasons[$i]['Game_season'] . "'>" . $seasons[$i]['Game_season'] . "-" . ($seasons[$i]['Game_season'] + 1) . "</option>";
        }

        // Automatically choose latest season
        echo 
        '
            <script>
                // Select latest season
                var seasonNum = document.getElementById("selectSeason").value=' . $latestSeason . ';
                seasonNum.selected = true;
            </script>
        ';
        ?>
        </select>
        <div class="input-group-append">
        <button class="btn btn-outline-secondary" id='viewButton' type="button" onclick="updateScheduleYear()">View</button>
        </div>
    </div>
    </div>
    <div class="col"></div>
    <div class="col"></div>
</div>