<?php require_once 'connect.php'; ?>
<?php
session_start();
include 'functions.php';

// if url has ?id=xx
if (isset($_GET['id'])) {

    // Holds id number of game
    $id = mysqli_real_escape_string($conn, $_GET['id']);

    // ID of this team
    $team_id = $_SESSION['team_id'];

    // Get game results
    $sql = "SELECT Game_team1, Game_team2, Game_team1score, Game_team2score, Game_date FROM game WHERE Game_id=$id LIMIT 1";
    $result = mysqli_query($conn, $sql); // Stores all table data in query
    $game = mysqli_fetch_all($result, MYSQLI_ASSOC); // Holds array of team

    $team1score = $game[0]['Game_team1score'];
    $team2score = $game[0]['Game_team2score'];
    $team1ID = $game[0]['Game_team1'];
    $team2ID = $game[0]['Game_team2'];
    $team1or2 = thisTeam($team1ID, $team2ID, $team_id);
    $g_date = $game[0]['Game_date'];

    $isLeagueGame = false;
    // Both teams exist
    if($team1ID != "" && $team2ID != "")
    {
        // Determines if a league game
        $teamSQL = "SELECT Team_league FROM team WHERE Team_id=$team1ID OR Team_id=$team2ID LIMIT 2";
        $teamResult = mysqli_fetch_all(mysqli_query($conn, $teamSQL));
        if($teamResult[0][0] == $teamResult[1][0])
        {
            $isLeagueGame = true;
        }
        echo "isLeagueGame: $isLeagueGame";
    }

    // Figure out season column
    $dateToTime = strtotime($g_date);
    $month = date("m", $dateToTime);
    $year = date("Y", $dateToTime);
    $season = substr($g_date, 0, 4); // Year from date
    if ($month < 7) {
        $season = $year - 1;
    } else {
        $season = $year;
    }

    // Ensure game has been played
    if ($team1or2 == 1 && ($team1score != '' && $team2score != '')) {

        $oppTeamID = $game[0]['Game_team2'];

        // Team wins, subtract win from record
        if ($team1score > $team2score) {

            // Update this team season
            $seasonSQL = "UPDATE season SET wins = wins - 1 WHERE season=$season AND team_id=$team_id";
            if($isLeagueGame)
            {
                $seasonSQL = "UPDATE season SET wins=wins-1, leagueWins=leagueWins-1 WHERE season=$season AND team_id=$team_id";
                $oppSeasonSQL = "UPDATE season SET losses=losses-1, leagueLosses=leagueLosses-1 WHERE season=$season AND team_id=$oppTeamID";
            }
            // Not league game but opponent exists in database
            elseif(!$isLeagueGame && $oppTeamID != "")
            {
                $oppSeasonSQL = "UPDATE season SET losses = losses - 1 WHERE season=$season AND team_id=$oppTeamID";
            }

        }
        // Team loses, subtract loss from record
        elseif ($team1score < $team2score) {

            $seasonSQL = "UPDATE season SET losses=losses-1 WHERE team_id=$team_id AND season=$season";
            if($isLeagueGame)
            {
                $seasonSQL = "UPDATE season SET losses=losses-1, leagueLosses=leagueLosses-1 WHERE team_id=$team_id AND season=$season";
                $oppSeasonSQL = "UPDATE season SET wins = wins - 1, leagueWins=leagueWins-1 WHERE season=$season AND team_id=$oppTeamID";
            }
            // Not league game but opponent exists
            elseif(!$isLeagueGame && $oppTeamID != "")
            {
                $oppSeasonSQL = "UPDATE season SET wins = wins - 1 WHERE season=$season AND team_id=$oppTeamID";
            }
        }
    }
    // Team ID is 2 and game was played 
    elseif ($team1or2 == 2 && ($team1score != '' && $team2score != '')) {
        $oppTeamID = $game[0]['Game_team2'];

        // Team wins
        if ($team2score > $team1score) {

            $seasonSQL = "UPDATE season SET wins = wins - 1 WHERE season=$season AND team_id=$team_id";
            if($isLeagueGame)
            {
                $seasonSQL = "UPDATE season SET wins = wins - 1, leagueWins=leagueWins-1 WHERE season=$season AND team_id=$team_id";
                $oppSeasonSQL = "UPDATE season SET losses = losses - 1, leagueLosses=leagueLosses-1 WHERE season=$season AND team_id=$oppTeamID";
            }

            // Opponent exists in database
            elseif(!$isLeagueGame && $oppTeamID != "")
            {
                $oppSeasonSQL = "UPDATE season SET losses = losses - 1 WHERE season=$season AND team_id=$oppTeamID";
            }
        }
        // Team loses
        elseif ($team2score < $team1score) {
            $seasonSQL = "UPDATE season SET losses = losses - 1 WHERE season=$season AND team_id=$team_id";
            if($isLeagueGame)
            {
                $seasonSQL = "UPDATE season SET losses = losses - 1, leagueLosses=leagueLosses-1 WHERE season=$season AND team_id=$team_id";
                $oppSeasonSQL = "UPDATE season SET wins = wins - 1, leagueWins=leagueWins-1 WHERE season=$season AND team_id=$oppTeamID";
            }
            elseif(!$isLeagueGame && $oppTeamID != "")
            {
                $oppSeasonSQL = "UPDATE season SET wins = wins - 1 WHERE season=$season AND team_id=$oppTeamID";
            }
        }
    }

    $sql3 = "DELETE FROM game WHERE Game_id=$id";

    if (mysqli_query($conn, $sql3) && mysqli_query($conn, $seasonSQL)) {
        echo "Game deleted & team record updated successfully<br>";

        // if opposing team exists
        if ($oppTeamID != '') {
            mysqli_query($conn, $oppSeasonSQL);
            echo "Record updated successfully";
        }
    } else {
        echo "Error deleting Game: " . mysqli_error($conn);
    }

    // Update W/L ratio
    $ratiosql = "UPDATE season SET wlratio = wins/(wins + losses), leagueWLRatio=leagueWins/(leagueWins+leagueLosses) WHERE season=$season AND team_id=$team_id";
    mysqli_query($conn, $ratiosql);

    // if opposing team exists
    if ($oppTeamID != '') {
        // Update W/L ratio
        $ratiosql2 = "UPDATE season SET wlratio = wins/(wins + losses), leagueWLRatio=leagueWins/(leagueWins+leagueLosses) WHERE season=$season AND team_id=$oppTeamID";
        mysqli_query($conn, $ratiosql2);
    }

    // Delete any season which has no games
    $emptySeasonSQL = "DELETE FROM season WHERE wins=0 AND losses=0";
    mysqli_query($conn, $emptySeasonSQL);
    
    // Redirect 
    header("location: dashboard.php");
    mysqli_close($conn);
}


?>