<!-- AJAX returns upcoming games in correct format depending on view (mobile/normal) -->
<?php require_once 'connect.php'; ?>
<?php

$type = $_GET["type"];

// Get current date
$dateObj = getdate();
$currYear = $dateObj['year'];
$currDay = sprintf("%02d", $dateObj['mday']);
$currMon = sprintf("%02d", $dateObj['mon']);
$dateVal = "$currYear-$currMon-$currDay"; // Outputs date in SQL format

// Display UP TO 10 games on/after current date
$gamesSQL = "SELECT * FROM game WHERE Game_date >= '$dateVal' ORDER BY Game_date ASC LIMIT 10";
$gamesResult = mysqli_query($conn, $gamesSQL);
$gamesArray = mysqli_fetch_all($gamesResult, MYSQLI_ASSOC);
?>

<?php

// Only display header if there are upcoming games
if (sizeof($gamesArray) != 0) {
  echo
    "
    <div class='container mt-4'>
    <h1 class='centerText'>Upcoming Games</h1>
    ";
}
?>
<!-- UPCOMING GAMES LIST -->
<table class="mt-3">
  <tbody>
    <?php
    include 'functions.php';
    $i = 0; // if even, echo a "tr", if odd, end with </tr> 

    // Loop through each upcoming games
    foreach ($gamesArray as $game) {

      // ID's for both teams
      $team1ID = $game['Game_team1'];
      $team2ID = $game['Game_team2'];

      $team2PhotoID = $team2ID; // Holds value of photo --> ex: 2.jpg or default.jpg

      // Beginning of links for team names
      /* $team1Link = "<a href='team.php?id=$team1ID'>";
      $team2Link = "<a href='team.php?id=$team2ID'>"; */

      // Beginning of links for team names
      $team1Link = "";
      $team2Link = "";

      // Both Team ID's exist
      if (is_numeric($team1ID) && is_numeric($team2ID)) {
        $teamSQL = "SELECT Team_name FROM team WHERE Team_id = $team1ID LIMIT 1";
        $teamResult = mysqli_query($conn, $teamSQL);
        $teamArray = mysqli_fetch_all($teamResult, MYSQLI_ASSOC); // Array holding team1 name

        $team2SQL = "SELECT Team_name FROM team WHERE Team_id = $team2ID LIMIT 1";
        $team2Result = mysqli_query($conn, $team2SQL);
        $team2Array = mysqli_fetch_all($team2Result, MYSQLI_ASSOC);

        $team1Name = $teamArray[0]['Team_name'];
        $team2Name = $team2Array[0]['Team_name'];

        $team1Link = "<a href='team.php?id=$team1ID'>$team1Name</a>";
        $team2Link = "<a href='team.php?id=$team2ID'>$team2Name</a>";
      }
      // Only Team 2 Exists
      elseif ($team1ID == "") {
        $teamSQL = "SELECT Team_name FROM team WHERE Team_id=$team2ID LIMIT 1";
        $teamResult = mysqli_query($conn, $teamSQL);
        $teamArray = mysqli_fetch_all($teamResult, MYSQLI_ASSOC); // Array holding team names

        $team2Name = $game['Game_oppName'];
        $team1Name = $teamArray[0]['Team_name'];

        $team1Link = "$team1Name";
        $team2Link = "<a href='team.php?id=$team2ID'>$team2Name</a>";
      }
      // Only Team 1 Exists
      elseif ($team2ID == "") {
        $teamSQL = "SELECT Team_name FROM team WHERE Team_id=$team1ID LIMIT 1";
        $teamResult = mysqli_query($conn, $teamSQL);
        $teamArray = mysqli_fetch_all($teamResult, MYSQLI_ASSOC); // Array holding team names

        $team1Name = $teamArray[0]['Team_name'];
        $team2Name = $game['Game_oppName'];
        $team2PhotoID = "default";
        
        $team1Link = "<a href='team.php?id=$team1ID'>$team1Name</a>";
        $team2Link = "$team2Name";
      }

      $gameDateForSchedule = formatDateForScheduleMinified($game['Game_date']);

      // ECHO GAME DATA

      // If counter is even
      if ($i % 2 == 0) {
        if ($type == 'normal') {

          echo "<tr><td class='half-width right-border'>";
        } else {
          echo "<tr><td class='half-width'>";
        }
      } else {
        echo '<td class="half-width">';
      }

      echo
        '
            <div style="outline:none" tabindex="0">
              <div class="cursor-pointer">
                <table class="table-layout-spacing">
                  <tr class="height-sm">
                    <td class="lc-np"></td>
                    <td></td>
                    <td class="width-six"></td>
                    <td class="date-formatting"></td>
                    <td class="width-med"></td>
                    <td class="no-padding" style="width: 0px;"></td>
                  </tr>
                  <tr>
                    <td class="alpha-padding" colspan="6"></td>
                  </tr>
                  <tr>
                    <td colspan="3"></td>
                    <td class="white-smoke-color" rowspan="5"></td>
                    <td rowspan="5" class="date-background-padding">
                      <div class="imspo_mt__ms-w">
                        <div class="imspo_mt__pm-inf">' . $gameDateForSchedule . '</div>
                        <div class="imspo_mt__ndl-p imspo_mt__pm-inf">7:00PM</div>
                      </div>
                    </td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                  </tr>
                  <tr class="imspo_mt__tr">
                    <td class="imspo_mt__lgc lc-np">
                      <img src="img/teamlogo/' . $team1ID . '.jpg" alt="" style="height: 24px; width: 24px;"></td>
                    <td class="tns-c imspo_mt__tt-w imspo_mt__dt-t">
                      <div class="ellipsisize">
                        <span>' . $team1Link . '</span>
                      </div>
                    </td>
                    <td class="width-six"></td>
                  </tr>
                  <tr class="imspo_mt__tr">
                    <td class="imspo_mt__lgc lc-np">
                      <img src="img/teamlogo/' . $team2PhotoID . '.jpg" alt="" style="height: 24px; width: 24px;"></td>
                    <td class="tns-c imspo_mt__tt-w imspo_mt__dt-t">
                      <div class="ellipsisize">
                        <span>' . $team2Link . '</span>
                      </div>
                    </td>
                    <td class="width-six"></td>
                  </tr>
                  <tr>
                    <td></td>
                  </tr>
                  <tr class="imspo_mt__br-s"></tr>
                </table>
              </div>
            </div>
          </td>
          ';


      $i++; // increment counter

      // New line
      if ($type == 'mobile' || $i % 2 == 0) {
        echo "</tr>";
      }
    }


    echo "</tbody></table>";

    ?>

    </div>
    </div>

    <?php echo "<hr>";
